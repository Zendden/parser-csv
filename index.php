<?php 
    use \Parser\Parser; 
    require_once ("csv_parser.php");
?>
<HTML>
    <HEAD>
        <title>CSV Parser V2</title>
        <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    </HEAD>
    <BODY style="background-color:aliceblue;">
        <div  class="container">
            <div class="card" style="margin-top:10px;">
                <div class="row" style="margin:2%;">
                    <div class="col">
                        <div class="form-group">
                            <form action="" method="POST" enctype="multipart/form-data">
                                <label for="inputFile">Choose CSV file: </label><br>
                                <input type="file" name="csv_file" id="inputFile"><br><br>
                                <input type="submit" name="parse" class="btn btn-primary">
                            </form>
                        </div>
                    </div>
                </div>
        
                <?php 
                    $file = $_FILES["csv_file"]["name"];
                    
                    if (!empty ($file)) {
                        
                        echo "<div class='col'>
                                <div class='card' style='padding: 15px; margin-top: 15px;'><br>Parsed file: <strong>".$file."</strong><br><hr><br>";

                        $Parser = new Parser ($file, ",");
                        $Parser->parseToCSV ($Parser->parseCSV ());
                        $Parser->sendCSV ("$this->createdAt.csv");

                        echo "</div></div>";
                    } else {
                        exit ();
                    }
                ?>
            </div>
        </div>
    </BODY>
</HTML>