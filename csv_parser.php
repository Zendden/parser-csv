<?php
declare(strict_types=1);
namespace Parser;

use Parser\vendor\symfony\routing\Annotation\Route;

class Parser {
    protected $csv_file; 
    protected $countArrayOne;
    protected $countArrayTwo;
    protected $countArrayThree;
    protected $csv_output_data = [];
    protected $devices = [];
    protected $delimiter;

    public function __construct ($csv_file, string $delimiter) {
        
        /**
         * File location and file name
         */
        $this->csv_file = file ($csv_file);

        /**
         * Разделитель
         */
        $this->delimiter = $delimiter;
    }

    public function parseCSV (): array {

        $this->countArrayOne = count ($this->csv_file);

        for ($i = 0; $i < $this->countArrayOne; $i++) {
            $this->csv_output_data[$i] = explode ($this->delimiter, $this->csv_file[$i]);
        }

        $this->countArrayTwo = count ($this->csv_output_data);
        for ($j = 0; $j < $this->countArrayTwo; $j++) {
            $this->devices[] = $this->csv_output_data[$j][4];
        }

        $this->devices = array_diff($this->devices, array(''));

        $devices = array_count_values($this->devices);

        arsort($devices);

        foreach ($devices as $device => $key) {
            echo "Device: ".$device.";<br>
            Marks: ".$key."<br><br>";
        }

        return $devices;
    }

    public function parseToCSV (array $data): void {

        $content = [];
        $createdAt = gmdate ("d.Y-H-i-s");

        if (count ($data) == 0) {
            die ("Array is empty");
        } else {
            $stream = fopen ("$createdAt.csv", "x");

            foreach ($data as $devices => $keys) {
                $content[] = [$devices, $keys];
            }

            for ($h = 0; $h < count ($content); $h++) {
                fputcsv ($stream, $content[$h], ",");
            }

            fclose ($stream);
        }
    }

    public function sendCSV ($filename) {
        
        $now = gmdate ("D, d M Y H:i:s");

        if (ob_get_level ()) {
            ob_end_clean ();
        }

        header('Content-Description: File Transfer');
        header('Content-Type: application/octet-stream');
        header('Content-Disposition: attachment; filename=' . $filename);
        header('Content-Transfer-Encoding: binary');
        header('Expires: 0');
        header('Cache-Control: must-revalidate');
        header('Pragma: public');

        readfile($filename);
        exit;
    }
}